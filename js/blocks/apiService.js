function get(url_p) {
    return new Promise ( (resolve, reject ) => {
        
        fetch(
            url+url_p,
            {   method: 'get',
                headers:{
                    'Content-Type': 'application/json',
                    'Authorization': getCookie('token')
                }
            }
        )
        .then( res => {
            

            UnAuthorizationErroradle(res.status, 'get', url_p);

            if (res.status === 200)
                return res.json()
            else
                return ({results: []})
        } )
        .catch(error => reject(error))
        .then(res => resolve(res.results));

    })
}


function peticion(url_p, method, body) {
    return new Promise ( (resolve, reject ) => {
        fetch(
            url_p,
            {   method ,
                body: JSON.stringify(body),
                headers:{
                    'Content-Type': 'application/json',
                    'Authorization': getCookie('token')
                }
            }
        ).then( res => {
            status = res.status
            
            UnAuthorizationErroradle(status, 'post', url_p);
            
            if (res.status !== 401)
                return res.json()
                .then(data => {
                    
                    return {status: status, res: data}
                    
                }).catch(error => ({status: status, res: {}}))
                
            else
                return ({status: status, res: {}})
        } ).catch(error => {alert('Ocurrió un error \n'+error); reject(error)})
        .then((re) => {
                resolve({status: re.status, res: re.res})
                
            

        });

    })
}


function UnAuthorizationErroradle(status, method, url_p) {
    

    
    if (status === 401) {   
        setCookie('token', '', 0);
        toggleModalLogin('show');
    }
    else if (
        (method === 'post') && url_p.includes(login_path) &&
        status !== 401 && 
        status !== "400" && 
        status !== 400 ) {
        
        toggleModalLogin('hide');
    }else if (
        (status === 200 || status === 201 || status === 204 || status === 404 ||
            status === "200" || status === "201" || status === "204" || status === "404")
        && method !== 'get') {
        actualizarLista();
    }
}