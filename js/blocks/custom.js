Blockly.Blocks['action'] = {
    init: function() {
        this.appendDummyInput()
            .appendField(new Blockly.FieldDropdown([["Crear","crear"], ["Borrar","borrar"], ["Editar","editar"]]), "action");
        this.appendValueInput("data")
            .setCheck("data");
        this.setColour(230);
    }
};

Blockly.Blocks['data'] = {
    init: function() {
        this.appendDummyInput()
            .appendField("Modelo")
            .appendField(new Blockly.FieldTextInput(""), "modelo");
        this.appendDummyInput()
            .appendField("URL")
            .appendField(new Blockly.FieldTextInput(""), "url");
        // this.appendDummyInput()
        //     .appendField("SI borras o editas, escribe el id ")
        //     .appendField(new Blockly.FieldNumber(), "id");
        this.setOutput(true, null);
        this.setColour(100);
    }
};


Blockly.JavaScript['data'] = function(block) {
    var input_model = block.getFieldValue('modelo');
    var input_url = block.getFieldValue('url');
    var input_id = block.getFieldValue('id');

    code = 'var input_model ="'+input_model+'";'
    code += 'var input_url ="'+input_url+'";'
    
    return [code, Blockly.JavaScript.ORDER_MEMBER]
}




Blockly.JavaScript['action'] = function(block) {
      
    var text = Blockly.JavaScript.valueToCode(block, 'data', Blockly.JavaScript.ORDER_ADDITION) || '0'
    eval(text);
  
    var input_action = block.getFieldValue('action');
    code = text + "var action = '"+input_action+"  '  + input_model;";

    return code
}
