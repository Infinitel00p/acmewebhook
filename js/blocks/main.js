function runCode() {
    window.LoopTrap = 1000;
    Blockly.JavaScript.INFINITE_LOOP_TRAP = 'if (--window.LoopTrap == 0) throw "Infinite loop.";\n';
    
    var code = Blockly.JavaScript.workspaceToCode(workspace);
    Blockly.JavaScript.INFINITE_LOOP_TRAP = null;
    return code;
}

function actualizarLista() {
    get('webhooks/').then(res => {
        $('#tbody').html('');
        if (res !== undefined)
        
            for (var i = 0; i < res.length; i++){
                $('#tbody').append(
                            '<tr class="btnSelect">'+
                            '<td scope="row">' + res[i].id + '</td>'+
                            '<td>' + res[i].action + '</td>'+
                            '<td>' + res[i].url + '</td>'+
                            '</tr>'
                    );
            }
    });
}



function guardar() {
    eval(runCode());
    peticion( url+'webhooks/', "post", 
        {
            action: action,
            url: input_url,
        }
    ).then( res => {
        webhookSelected = undefined;
        alertStatus(res.status);
    });
}

function borrar() {
    if (webhookSelected === undefined || webhookSelected === null){
        alert('Selecciona un webhook para eliminar');
        return;
    } 

    peticion( url+'webhooks/'+webhookSelected.id+'/', "delete", {})
    .then( res => {
        webhookSelected = undefined;
        alertStatus(res.status);
    })
    .catch(res => webhookSelected = undefined);
}

function editar() {
    if (webhookSelected === undefined || webhookSelected === null){
        alert('Selecciona un webhook para editar');
        return;
    } 

    eval(runCode());

    if (input_model === "" && input_url ===""){
        alert("Debe llenar algunas de los campos para poder editar su valor")
        return;
    }

    if (input_model !== undefined && input_model !== '' && input_model !== null)
        webhookSelected.action = action;
    
    
    if (input_url !== undefined && input_url !== '' && input_url !== null)
        webhookSelected.url = input_url;
    
    
    peticion( url+'webhooks/'+webhookSelected.id+'/', "put", 
        {
            action: webhookSelected.action,
            url: webhookSelected.url
        }
    )
    .then( res => {
        webhookSelected = undefined;
        alertStatus(res.status);
    })
    .catch(res => webhookSelected = undefined);
}




function alertStatus(status){
    switch (status) {
        case "200":
            alert('Webhook editado con éxito');
            break;
    
        case "201":
            alert('Webhook creado con éxito');
            break;
    
        case "404":
            alert('Webhook no encontrado ');
            break;

        case "204":
            alert('Webhook borrado con éxito ');
            break;

        case "401":
            alert('Debes iniciar sesión')
            toggleModalLogin('show');
            break;
        default:
            break;
    }
}