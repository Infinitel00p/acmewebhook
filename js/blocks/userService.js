function login(data) {
    peticion(url+login_path, 'post', data, false )
    .then( r => {
            if (r.res.non_field_errors){
                alert(r.res.non_field_errors[0]);
                return;
            }
            else if (!r.res.token) {
                alert('Deben llenar todos los campos')
                return;
            }
            else if (r.res.token){
                setCookie('token', 'token '+r.res.token, 1);
                actualizarLista();
                toggleModalLogin('hide');  
                return;
            }

    });
}

function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays*24*60*60*1000));
    var expires = "expires="+ d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}

function getCookie(cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for(var i = 0; i <ca.length; i++) {
      var c = ca[i];
      while (c.charAt(0) == ' ') {
        c = c.substring(1);
      }
      if (c.indexOf(name) == 0) {
        return c.substring(name.length, c.length);
      }
    }

    return "";
}