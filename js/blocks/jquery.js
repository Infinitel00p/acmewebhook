

$(document).ready(function(){

    // Leer los datos de la fila selecionado y guardarlo en la var webhookSelected 
    $("table").on('click','.btnSelect',function(){
        // Obtener fila actual
        var currentRow=$(this).closest("tr"); 
        
        var col1=currentRow.find("td:eq(0)").text(); 
        var col2=currentRow.find("td:eq(1)").text(); 
        var col3=currentRow.find("td:eq(2)").text();
        webhookSelected = {
            id: col1,
            action: col2,
            url: col3,
        }

    });


    $("tbody").on('click', 'tr',(function () {
        $('.selected').removeClass('selected');
        $(this).addClass("selected");
    }));

    
    $('#login').submit(function(e){
        e.preventDefault()
        var formData = new FormData(document.querySelector('form'));
        
        login({
            username: formData.get('username'),
            password: formData.get('password'),
        });
    })

    // if(getCookie('token') === ""){
    //     toggleModalLogin('show');

        actualizarLista();
});



function toggleModalLogin(toggle) { 
    
    $('#loginModal').modal(toggle);
}